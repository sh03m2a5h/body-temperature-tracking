import { TemperatureData } from "./types"

export const arr2Data = (arr: string[][]): TemperatureData[] => {
    const today = new Date()
    return arr.map(([time, temp]) => {
        const date = new Date(today)
        const [hours, minutes, seconds] = time.split(':').map(s => Number(s))
        date.setHours(hours, minutes, seconds)
        return {
            date: date,
            temperature: Number(temp)
        }
    })
}

export const movingAvg = (data: TemperatureData[], length: number = 12): TemperatureData[] => {
    const avgd = data.map((d, i) => {
        const prvs = data.slice(i > length ? i - length: 0, i)
        return {
            date: d.date,
            temperature: prvs.reduce((p, d) => p + d.temperature,0) / prvs.length
        }
    })
    return avgd
}
