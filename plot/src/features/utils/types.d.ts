export type TemperatureData = {
    date: Date
    temperature: number
}
