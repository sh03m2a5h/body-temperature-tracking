import React, { DragEvent, useState } from 'react'
import { DropAreaProps } from './DropAreaProps'

export const DropArea: React.FC<DropAreaProps> = ({ onDrop }) => {
  const [active, setActive] = useState(false)

  const handleDragOver = (e: DragEvent<HTMLDivElement>) => {
    e.stopPropagation()
    e.preventDefault()
    setActive(true)
  }

  const handleDragLeave = (e: DragEvent<HTMLDivElement>) => {
    e.stopPropagation()
    e.preventDefault()
    setActive(false)
  }

  const handleDrop = (e: DragEvent<HTMLDivElement>) => {
    e.stopPropagation()
    e.preventDefault()

    setActive(false)
    onDrop?.(e)
  }

  return (
    <div
      css={[
        {
          width: 400,
          height: 200,
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          border: '3px dashed gray',
          borderRadius: 20,
        },
        active && {
          border: '3px dashed #9CD6FF',
        },
      ]}
      onDragOver={handleDragOver}
      onDragLeave={handleDragLeave}
      onDrop={handleDrop}
    >
      <span css={[{
        color: "black"
      }, active && {
        color: '#9CD6FF',
      }]}>ファイルをここにドロップ</span>
      <span css={[{
        color: "gray"
      }, active && {
        color: '#9CD6FF',
      }]}>(.csv)</span>
    </div>
  )
}
