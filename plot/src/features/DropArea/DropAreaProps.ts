import type { DragEvent } from 'react'

export type DropAreaProps = {
  onDrop?: (e: DragEvent<HTMLDivElement>) => void
}
