import { TemperatureData } from "../utils/types";

export type ChartProps = {
    data: TemperatureData[]
}
