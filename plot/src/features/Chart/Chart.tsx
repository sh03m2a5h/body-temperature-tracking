import * as am4core from '@amcharts/amcharts4/core'
import * as am4charts from '@amcharts/amcharts4/charts'
import am4themes_animated from '@amcharts/amcharts4/themes/animated'
import React, { useEffect } from 'react'
import { ChartProps } from './ChartProps'

export const Chart: React.FC<ChartProps> = ({ data }) => {
  useEffect(() => {
    am4core.useTheme(am4themes_animated)

    const chart = am4core.create('chartdiv', am4charts.XYChart)
    chart.data = data.slice(1, data.length - 1)

    // Create axes
    let dateAxis = chart.xAxes.push(new am4charts.DateAxis())
    dateAxis.renderer.minGridDistance = 50

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis())

    // Create series
    const series = chart.series.push(new am4charts.LineSeries())
    series.dataFields.valueY = 'temperature'
    series.dataFields.dateX = 'date'
    series.strokeWidth = 2
    // series.tensionX = 1
    series.minBulletDistance = 10
    series.tooltipText = '{valueY}°C'
    if (series.tooltip) {
      series.tooltip.pointerOrientation = 'vertical'
      series.tooltip.background.cornerRadius = 20
      series.tooltip.background.fillOpacity = 0.5
      series.tooltip.label.padding(12, 12, 12, 12)
    }

    // Add scrollbar
    chart.scrollbarX = new am4charts.XYChartScrollbar()
    ;(chart.scrollbarX as am4charts.XYChartScrollbar).series.push(series)

    // Add cursor
    chart.cursor = new am4charts.XYCursor()
    chart.cursor.xAxis = dateAxis
    chart.cursor.snapToSeries = series
    return () => {
      chart.dispose()
    }
  }, [])
  return (
    <div css={{
      width: '100vw',
      height: '100vh',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }}>
      <div id="chartdiv" css={{
        width: '90%', height: '90%',
        border: '2px solid #DDD',
        borderRadius: 20,
        padding: '10px',
      }}></div>
    </div>
  )
}
