import React from "react"
import { DropArea } from "../DropArea/DropArea"
import { FileDropProps } from "./FileDropProps"

export const FileDrop: React.FC<FileDropProps> = ({ onDrop }) => {

    const handleDrop = async (e: React.DragEvent<HTMLDivElement>) => {
        const item = e.dataTransfer.items[0]
        const file = item.getAsFile()

        if(file) {
            const text = await file.text()
            const rows = text.split('\n').map(r => r.trim().split(','))
            onDrop?.(rows)
        }
    }

    return (
        <div css={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            margin: '10px'
          }}>
            <DropArea onDrop={handleDrop} />
        </div>
    )
}
