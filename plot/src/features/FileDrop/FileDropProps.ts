export type FileDropProps = {
  onDrop?: (rows: string[][]) => void
}
