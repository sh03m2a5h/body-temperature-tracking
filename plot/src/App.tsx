import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { FileDrop } from './features/FileDrop/FileDrop';
import { TemperatureData } from './features/utils/types';
import { arr2Data, movingAvg } from './features/utils/dataConverter';
import { Chart } from './features/Chart/Chart';

function App() {
  const [data, setData] = useState<TemperatureData[]|null>(null)
  const handleDrop = (rows: string[][]) => {
    console.log(rows)
    setData(movingAvg(arr2Data(rows)))
  }
  return (
    <div className="App">
      {
        !data ? 
          <FileDrop onDrop={handleDrop}/>
          :
          <Chart data={data}/>
      }
    </div>
  );
}

export default App;
