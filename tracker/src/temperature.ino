#include <Arduino.h>

#include <WiFi.h>
#include <WiFiMulti.h>

#include <HTTPClient.h>

#include <OneWire.h>
#include <DallasTemperature.h>
#include <M5Core2.h>

#include <wifis.ino>

// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 33

WiFiMulti wifiMulti;

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

void setup(void)
{
  // start serial port
  Serial.begin(115200);
  Serial.println("Dallas Temperature IC Control Library Demo");

  delay(3000);

  for(int i = 0; i < wifisLen; ++i){
    wifiMulti.addAP(wifis[i].ssid, wifis[i].pass);
  }
  // Start up the library
  // sensors.begin();
  M5.begin(true, true, true, false, kMBusModeOutput);
  M5.Lcd.setBrightness(0);
  M5.Rtc.begin();
  // SD.begin();
}

void loop(void)
{ 
  sensors.begin();
  delay(10);
  sensors.requestTemperatures(); // Send the command to get temperatures

  String dateStr = getDateStr();
  String timeStr = getTimeStr();
  
  File f = SD.open("/" + dateStr + ".csv", FILE_APPEND);
  String temperature = String(sensors.getTempCByIndex(0), 2);
  f.println(timeStr + "," + temperature);
  Serial.println(timeStr + " " + temperature);

  f.close();

  if(M5.Axp.isACIN()) {
    delay(5000);
    if(wifiMulti.run()) {
    }
  } else {
    M5.Axp.LightSleep(5000000);
  }
}

String getTimeStr() {
  RTC_TimeTypeDef timeStruct;
  M5.Rtc.GetTime(&timeStruct);
  char buf[20];

  sprintf(buf, "%02d:%02d:%02d",
            timeStruct.Hours,
            timeStruct.Minutes,
            timeStruct.Seconds);
  return buf;
}

String getDateStr() {
  RTC_DateTypeDef dateStruct;
  M5.Rtc.GetDate(&dateStruct);
  char buf[20];

  sprintf(buf, "%d-%02d-%02d",
            dateStruct.Year,
            dateStruct.Month,
            dateStruct.Date);
  return buf;
}
